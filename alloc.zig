const std = @import("std");
const mem = std.mem;

// zig build-lib -target wasm32-freestanding -mcpu=bleeding_edge -flto -fno-compiler-rt -dynamic -O ReleaseSmall alloc.zig

var gpa: std.heap.GeneralPurposeAllocator(.{}) = undefined;
var backing_allocator: mem.Allocator = undefined;
var allocation_sizes: std.AutoHashMap(usize, usize) = undefined;

export fn allocator_init() callconv(.C) void {
    gpa = std.heap.GeneralPurposeAllocator(.{}){};
    backing_allocator = gpa.allocator();
    allocation_sizes = std.AutoHashMap(usize, usize).init(backing_allocator);
}

export fn allocator_deinit() callconv(.C) void {
    allocation_sizes.deinit();
    _ = gpa.deinit();
}

export fn malloc(size: usize) callconv(.C) ?*c_void {
    const allocation = backing_allocator.alloc(u8, size) catch {
        return null;
    };
    allocation_sizes.put(@ptrToInt(allocation.ptr), size) catch {
        backing_allocator.free(allocation);
        return null;
    };
    return allocation.ptr;
}

export fn free(pre_ptr: ?*c_void) callconv(.C) void {
    const ptr = @ptrCast([*]u8, pre_ptr orelse return);
    const size_entry = allocation_sizes.fetchRemove(@ptrToInt(ptr)) orelse return;
    const slice = ptr[0..size_entry.value];
    backing_allocator.free(slice);
}

fn getSize(ptr: *c_void) ?usize {
    return allocation_sizes.get(@ptrToInt(ptr));
}

fn getSlice(ptr: *c_void) ?[]u8 {
    const size = getSize(ptr) orelse return null;
    return @ptrCast([*]u8, ptr)[0..size];
}

export fn realloc(pre_ptr: ?*c_void, new_size: usize) callconv(.C) ?*c_void {
    const ptr = pre_ptr orelse return null;
    const old_slice = getSlice(ptr) orelse return null;
    const new_allocation = backing_allocator.realloc(old_slice, new_size) catch {
        return null;
    };
    // We can assume capacity here because we know the allocation is already
    // in the table.
    allocation_sizes.putAssumeCapacity(@ptrToInt(ptr), new_size);
    return new_allocation.ptr;
}
