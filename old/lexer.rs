use logos::Logos;

#[derive(Logos, Debug, PartialEq)]
pub enum RawToken {
    #[error]
    // Skip whitespace
    #[regex(r"\s+", logos::skip, priority = 4)]
    Error,

    #[token("(")]
    OpenParen,
    #[token(")")]
    CloseParen,

    #[regex(r";;.*\n")]
    LineComment,

    #[token("(;")]
    OpenBlockComment,
    #[token(";)")]
    CloseBlockComment,

    #[regex(
        r#"(?x)
        "(
            [^"\\]
            | \\ (
                [0-9a-fA-F][0-9a-fA-F]
                | [tnr"'\\]
                | u\{[0-9a-fA-F]+\}
            )
        )*"
        "#
    )]
    String,

    #[regex(r"[+-]?[0-9]+", priority = 3)]
    DecimalInteger,

    #[regex(r#"[^\(\)";\s]+"#, priority = 2)]
    Identifier,
}

#[derive()]
pub struct Lexer<'source> {
    logos: logos::Lexer<'source, RawToken>,
}

impl Lexer<'_> {
    pub fn lex(source: &str) -> Vec<RawToken> {
        RawToken::lexer(source).collect()
    }

    pub fn next() {}
}
