# Ideas

~~Should probably just use manual memory management. Actually make a better
distinction beween stack/local values and memory values, The semantics are
different. Maybe stuff shouldn't automatically promote to a memory value by
accident.~~ This is fleshed out now

I want the language to feel close to wasm/wat. Just as C feels close to ASM.

~~Some sort of static multiple dispatch would be cool.~~ we're gonna do
typeclasses/traits instead

Type checked inline asm

zig-style fancy pointers? not only have information about the type but about quantity and storage
    as long as they are thin pointers, we can still subtype them to normal pointers

what if my only code primitve was inline asm and declaring functions and i wrote the whole stdlib on that
    that won't work, but i can still do a lot of the stdlib like that

Qualified name resolution, don't work yet in variable references
but for imports and stuff, we can use space separated names

so stuff like `(import std ops Add)`

Maybe switch to `wasmtime` wasm runtime instead of `wasmer`


## Memory model
Wasm's memory model presents some unique challenges

## Possible components
- Wasm linker to staticly link several modules together
- Reimplement wat2wasm & wasm2wat

## Inspirations
- [Rust](https://www.rust-lang.org/)
- [Zig](https://ziglang.org/)
- [Carp](https://github.com/carp-lang/Carp)
- [Grain](https://grain-lang.org/)
- [R7RS](https://small.r7rs.org/attachment/r7rs.pdf)

## Resources
- [Wasm Spec](https://webassembly.github.io/spec/core/index.html)
- [Wasm Proposals](https://github.com/WebAssembly/proposals)
- [Rust langdev](https://github.com/Kixiron/rust-langdev)

## Tools
- [Wat2wasm demo](https://webassembly.github.io/wabt/demo/wat2wasm/)

## Implementation details
- constant analysis happens in codegen
    - constant analysis to differentiate between direct function calls and indirect function calls

### Type system
- https://dl.acm.org/doi/pdf/10.1145/345099.345100
- https://arxiv.org/pdf/1306.6032.pdf
- https://www2.ccs.neu.edu/racket/pubs/esop09-sthf.pdf

- concrete types are subtypes of traits they implement
- intersection types are generic requirements with mulitple traits

- perhaps have `#[repr(transparent)]` equivalent subtype as what it is transparent for

- Would i do this by an interface? Maybe it will be a special supertype with
  type parameters for param and result
- make a supertype for callable things
  subtypes would be functions, labels, and closures(?)
  (idk how closures would work yet, but they would be a subtype of callable and
  not a function)

- Traits are a set of items (functions, constants, types) that can be defined for a type

- Compile time only types? such as labels to branch to on call syntax
    - Can maybe implement it as Non-primitivizable types
    - Probably does not mesh well with my compiletime JIT compliation idea


### Compiler phases
- Parse code to SExprs
- Parse code to AST (syntactically valid, unresolved types)
- convert code to MIR (semanically valid, everything should be concrete types, functions should be monomorphized)
    - All types must be concrete types
- convert code to LIR (primitivised types)
    - optimize LIR with `egg`
        - inlining
        - const evaluation
        - mathematical rewrites
    - May want the optimizer to work directly with WASM instruction.
        - It's probably structured enough to work well.
        - Paper about optimizing SSA with E-PEG: https://doi.org/10.1145/1480881.1480915
- convert code to WAT
- convert code to WASM with wat2wasm

#### LIR
What if the LIR had a partial evaluation type system structure

Maybe i should use an existing library for LIR

# Other

polymorphic identity function (single)
`∀α. α -> α`
(∀ a (func (a) (a))

(for ((... Ts)) (func ((... Ts)) ((... Ts))))





```
(closure (i32) (i32)) <: (callable (i32) (i32))

closure: ∀s,a,b. (s, (s,a -> b))

(variadic i32 i32 i32 i32)


concrete traits are functions from one type to trait contents



generic traits are functions from generic parameters to concrete trait


T: Add<T> means Add<T> T

(def-type closure (s a b)
    (struct
        state s
        func (func-ref (, s a) (b))))

struct Closure<s,a,b> {
    state: s,
    func: fn(s,a) -> b,
}





(impl ((A) (B)) (callable A B) (funcref A B)
    (def-func call ((self (funcref A B)) (input A)) (B)
        ))

(impl ((A) (B) (S)) (callable A B) (closure S A B)
    (def-func call ((self (closure S A B)) (input A)) (B)
        ((closure-func self) (closure-state self) input)))




(struct (point)
    x f32
    y f32)

;; or

(struct (ppoint T)
    (x T)
    (y T))

(def-type point (ppoint f32))


Varadic =






chain : ∀a,x̄,b. (-> (a (-> x̄ᵢ x̄ᵢ₊₁) (x̄ⱼ b)) b)



((T) (U)) chain ((a T) (f1 (func (T) (U))) (fs )

chain a f1 fs


(def-type func-chain )





func-chain is a GADT

func-chain a b:
| nil
| cons (func (a) (i)) (func-chain (i) (b))


(cons i32 (cons i32 (nil)))

(var i32 i32)


(def-func-variadic (V) values (vs (V)) (V)
    vs)




(def-func (drop . vs) (∀ ((... V)) (-> V ()))
    (values))


) )


∀


(func (i32) (i32)) <: (callable (i32) (i32))


```
