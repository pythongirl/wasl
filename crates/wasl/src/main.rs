use miette::Result;

#[macro_use]
mod common;
#[macro_use]
mod sexpr;
mod compiler;
mod types;

use compiler::ast;
use compiler::lir;
use compiler::mir;
use compiler::scope;

const PROGRAM: &str = r#"
;; (: 250 i8)

;; (for (a b) (-> ((-> (a) (b)) a) (b1))


;; ((i32 -> i32) i32 -> i32)
(def-func (apply f a) (func ((func (i32) (i32)) i32) (i32))
    (f a))
;; (export apply)

(def-func (+ a b) (func (i32 i32) (i32))
    (%inline-asm (i32 i32) (i32) ()
        (i32.add)
        a b))
(export +)

(def-const SEVEN i32 (+ 3 4))

(def-func (get-seven) (func () (i32))
    SEVEN)

(export get-seven)

(export SEVEN)

(def-global COUNTER i32 0)

(def-func (test-set) (func () ())
    (set! COUNTER (+ 1 COUNTER)))

(export test-set)

"#;

fn main() -> Result<()> {
    // let tokens = lexer::Lexer::lex(PROGRAM);
    // dbg!(tokens);

    let interner = compiler::interner::Interner::new();

    let builtins = scope::Scope::make_builtins_scope(&interner);

    let program = match sexpr::parse(PROGRAM, &interner.clone()) {
        Ok(program) => program,
        Err(sexpr_error) => {
            println!("Found error while parsing file");
            println!("{:?}", sexpr_error);
            return Ok(());
        }
    };
    // dbg!(&program);

    // let test_sexpr = &program[0];

    // test_sexpr
    //     .write(&mut std::io::stdout(), &parser_context.interner)
    //     .whatever_context("error while writing expression")?;
    // println!();

    let parser_context = ast::ParserContext {
        interner: interner.clone(),
    };

    let hir_module = match ast::Module::parse(&program, &parser_context) {
        Ok(e) => e,
        Err(err) => {
            println!("Error while parsing module");
            println!("{:?}", err);
            return Ok(());
        }
    };
    // dbg!(&hir_module);

    let mir_module = mir::Module::from_hir(&builtins, &interner, &hir_module).unwrap();
    // dbg!(&mir_module);

    let mut lir_module = lir::Module::from_mir(&interner, &mir_module);

    // dbg!(&lir_module);

    // let generated = codegen::ModuleContext::generate_module(interner.clone(), &mir_module).unwrap();

    // dbg!(&lir_module

    let generated = lir_module.output_module();
    generated
        .write(&mut std::io::stdout(), &interner)
        .expect("error while writing expression");
    println!();
    println!();

    lir_module.optimize();

    let generated = lir_module.output_module();
    generated
        .write(&mut std::io::stdout(), &interner)
        .expect("error while writing expression");
    println!();

    // let func_def = match &mir_module.items[&plus_symbol].data {
    //     mir::ModuleItemKind::Function(func) => func,
    //     _ => panic!(),
    // };

    // let generated = codegen::FuncContext::generate_func(func_def, &mut interner).unwrap();

    // generated
    //     .write(&mut std::io::stdout(), &interner)
    //     .whatever_context("error while writing expression")?;
    // println!();

    // let const_def = match &mir_module.items[&interner.get_or_intern("SEVEN")].data {
    //     mir::ModuleItemKind::Constant(c) => c,
    //     _ => panic!(),
    // };

    // let mut ctx = codegen::FuncContext::new(&mut interner);
    // let generated = ctx.generate_expr(&const_def.body).unwrap();

    Ok(())
}
