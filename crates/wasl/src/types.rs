use derive_more::From;
use num::ToPrimitive;

pub mod primitive;

pub trait Type {
    fn as_primitive(&self) -> primitive::ResultType;
}

macro_rules! add_const_shortcut {
    ($p:ty, $c:ty, $f:path; $($t:ident),+) => {
        impl $p {
            $(pub const $t: $p = $f(<$c>::$t);)*
        }
    };
}

// #[derive(Clone, Debug)]
// pub struct StructType {
//     fields: Vec<(Spur, MemoryType)>,
// }

#[derive(Clone, Debug, PartialEq)]
pub enum IntegerType {
    U8,
    I8,
    U16,
    I16,
    U32,
    I32,
    U64,
    I64,
}

#[derive(Clone, Debug, PartialEq)]
pub enum FloatType {
    F32,
    F64,
}

// #[derive(Debug)]
// pub struct Array {
//     object: MemoryType,
//     size: usize,
// }

// #[derive(Debug)]
// pub struct PointerType {
//     pointee: Box<MemoryType>,
// }

#[derive(Clone, Debug, From, PartialEq)]
pub enum MemoryType {
    Integer(IntegerType),
    Float(FloatType),
    // Pointer(PointerType),
    // Boolean,
    // Struct(StructType),
}

impl Type for MemoryType {
    fn as_primitive(&self) -> primitive::ResultType {
        match self {
            MemoryType::Integer(IntegerType::I32) => {
                primitive::ResultType::from(primitive::ValueType::I32)
            }
            MemoryType::Integer(IntegerType::I64) => {
                primitive::ResultType::from(primitive::ValueType::I64)
            }
            MemoryType::Float(FloatType::F32) => {
                primitive::ResultType::from(primitive::ValueType::F32)
            }
            MemoryType::Float(FloatType::F64) => {
                primitive::ResultType::from(primitive::ValueType::F64)
            }
            _ => unimplemented!(),
        }
    }
}

add_const_shortcut!(MemoryType, IntegerType, MemoryType::Integer; I8, U8, I16, U16, I32, U32, I64, U64);
add_const_shortcut!(MemoryType, FloatType, MemoryType::Float; F32, F64);

#[derive(Clone, Debug, PartialEq)]
pub struct FunctionReferenceType {
    pub param: Values,
    pub result: Values,
}

impl Type for FunctionReferenceType {
    fn as_primitive(&self) -> primitive::ResultType {
        // let func = Syntax::from(interner.borrow().get(PRIMITIVE_FUNC_SYMBOL).unwrap());
        // let param = Syntax::from(interner.borrow().get(PRIMITIVE_PARAM_SYMBOL).unwrap());
        // let result = Syntax::from(interner.borrow().get(PRIMITIVE_RESULT_SYMBOL).unwrap());

        // let mut param_part = vec![param];
        // param_part.extend_from_slice(self.param.as_primitive(interner)?.as_list()?);
        // let param_part = Syntax::from(SExpr::List(param_part));

        // let mut result_part = vec![result];
        // result_part.extend_from_slice(self.result.as_primitive(interner)?.as_list()?);
        // let result_part = Syntax::from(SExpr::List(result_part));

        // Some(Syntax::from(SExpr::from(vec![
        //     func,
        //     param_part,
        //     result_part,
        // ])))
        primitive::ResultType::from(primitive::ValueType::FuncRef)
    }
}

#[derive(Clone, Debug, From, PartialEq)]
pub enum TableType {
    FuncRef(FunctionReferenceType),
    ExternRef,
}

impl Type for TableType {
    fn as_primitive(&self) -> primitive::ResultType {
        match self {
            TableType::FuncRef(_) => primitive::ResultType::from(primitive::ValueType::FuncRef),
            TableType::ExternRef => primitive::ResultType::from(primitive::ValueType::ExternRef),
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum LiteralType {
    Integer(num::BigInt),
    Float(f64),
}

#[derive(Clone, Debug, From, PartialEq)]
pub enum LocalType {
    Memory(MemoryType),
    Table(TableType),
    Literal(LiteralType),
    Tuple(Vec<LocalType>),
    Never, // AKA Bottom
}

impl From<FunctionReferenceType> for LocalType {
    fn from(func_type: FunctionReferenceType) -> Self {
        LocalType::Table(TableType::FuncRef(func_type))
    }
}

impl Type for LocalType {
    fn as_primitive(&self) -> primitive::ResultType {
        match self {
            LocalType::Memory(m) => m.as_primitive(),
            LocalType::Table(t) => t.as_primitive(),
            LocalType::Tuple(ts) => ts.iter().flat_map(|f| f.as_primitive()).collect(),
            LocalType::Never => primitive::ResultType::UNIT,
            _ => unimplemented!(),
        }
    }
}

add_const_shortcut!(LocalType, MemoryType, LocalType::Memory; I8, U8, I16, U16, I32, U32, I64, U64, F32, F64);

impl LocalType {
    pub const EXTERN_REF: LocalType = LocalType::Table(TableType::ExternRef);
}

// TODO: maybe add another higher type that is for types allowed ast comptime interpretation

#[derive(Clone, Debug, PartialEq)]
pub struct Values {
    pub values: Box<[LocalType]>,
}

impl AsRef<[LocalType]> for Values {
    fn as_ref(&self) -> &[LocalType] {
        self.values.as_ref()
    }
}

impl AsMut<[LocalType]> for Values {
    fn as_mut(&mut self) -> &mut [LocalType] {
        self.values.as_mut()
    }
}

impl FromIterator<LocalType> for Values {
    fn from_iter<T: IntoIterator<Item = LocalType>>(iter: T) -> Self {
        let type_vec = iter.into_iter().collect::<Vec<LocalType>>();
        Values {
            values: type_vec.into_boxed_slice(),
        }
    }
}

impl From<LocalType> for Values {
    fn from(t: LocalType) -> Self {
        Values::from_single(t)
    }
}

impl Values {
    pub fn unit() -> Values {
        Values {
            values: Box::new([]),
        }
    }

    pub fn new<const N: usize>(values: [LocalType; N]) -> Values {
        Values {
            values: Box::from(values),
        }
    }

    pub fn from_single(t: LocalType) -> Values {
        Values::new([t])
    }

    pub fn len(&self) -> usize {
        self.values.len()
    }

    pub fn as_single(&self) -> Option<LocalType> {
        match self.values.as_ref() {
            [t] => Some(t.clone()),
            _ => None,
        }
    }
}

impl Type for Values {
    fn as_primitive(&self) -> primitive::ResultType {
        self.values.iter().flat_map(|t| t.as_primitive()).collect()
    }
}

// Subtyping stuff

impl PartialOrd for LocalType {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        use std::cmp::Ordering;
        match (
            is_subtype_single(self, other),
            is_subtype_single(other, self),
        ) {
            (true, true) => Some(Ordering::Equal),
            (true, false) => Some(Ordering::Less),
            (false, true) => Some(Ordering::Greater),
            (false, false) => None,
        }
    }
}

fn is_subtype_single(a: &LocalType, b: &LocalType) -> bool {
    match (a, b) {
        (a, b) if a == b => true,
        // Bottom rule
        (&LocalType::Never, _) => true,
        // TODO: add Top rule
        // (_, &Type::Top) => true,

        // Function subtyping
        (LocalType::Table(TableType::FuncRef(f)), LocalType::Table(TableType::FuncRef(g))) => {
            f.param >= g.param && f.result <= g.result
        }

        // Literals subtyping
        (LocalType::Literal(LiteralType::Integer(n)), l) => match l {
            LocalType::Memory(MemoryType::Integer(IntegerType::I8)) => n.to_i8().is_some(),
            LocalType::Memory(MemoryType::Integer(IntegerType::U8)) => n.to_u8().is_some(),
            LocalType::Memory(MemoryType::Integer(IntegerType::I16)) => n.to_i16().is_some(),
            LocalType::Memory(MemoryType::Integer(IntegerType::U16)) => n.to_u16().is_some(),
            LocalType::Memory(MemoryType::Integer(IntegerType::I32)) => n.to_i32().is_some(),
            LocalType::Memory(MemoryType::Integer(IntegerType::U32)) => n.to_u32().is_some(),
            LocalType::Memory(MemoryType::Integer(IntegerType::I64)) => n.to_i64().is_some(),
            LocalType::Memory(MemoryType::Integer(IntegerType::U64)) => n.to_u64().is_some(),
            _ => false,
        },
        (LocalType::Literal(LiteralType::Float(_)), LocalType::Memory(MemoryType::Float(_))) => {
            true
        }
        // Everything else false
        _ => false,
    }
}

fn is_subtype_multi(a: &Values, b: &Values) -> bool {
    a.len() == b.len()
        && a.as_ref()
            .iter()
            .zip(b.as_ref().iter())
            .all(|(x, y)| x <= y)
}

impl PartialOrd for Values {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        use std::cmp::Ordering;
        match (is_subtype_multi(self, other), is_subtype_multi(other, self)) {
            (true, true) => Some(Ordering::Equal),
            (true, false) => Some(Ordering::Less),
            (false, true) => Some(Ordering::Greater),
            (false, false) => None,
        }
    }
}

pub const BUILTIN_TYPES: &[(&str, LocalType)] = &[
    ("i8", LocalType::I8),
    ("u8", LocalType::U8),
    ("i16", LocalType::I16),
    ("u16", LocalType::U16),
    ("i32", LocalType::I32),
    ("u32", LocalType::U32),
    ("i64", LocalType::I64),
    ("u64", LocalType::U64),
    ("f32", LocalType::F32),
    ("f64", LocalType::F64),
    ("!", LocalType::Never),
];
