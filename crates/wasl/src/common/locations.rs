pub use codespan::Span;

pub trait AsLocation {
    fn as_location(&self) -> Location;
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Location(Span);

impl Location {
    pub const fn with<T>(self, data: T) -> Locatable<T> {
        Locatable {
            data,
            location: self,
        }
    }

    pub fn wrapper<T>(&self) -> impl Fn(T) -> Locatable<T> + '_ {
        |d| self.with(d)
    }

    pub fn merge(self, other: Location) -> Location {
        Location(self.0.merge(other.0))
    }
}

impl From<pest::Span<'_>> for Location {
    fn from(pest_span: pest::Span) -> Location {
        Location(Span::new(pest_span.start() as u32, pest_span.end() as u32))
    }
}

impl FromIterator<Location> for Option<Location> {
    fn from_iter<T: IntoIterator<Item = Location>>(iter: T) -> Self {
        iter.into_iter().reduce(Location::merge)
    }
}

impl AsLocation for Location {
    fn as_location(&self) -> Location {
        *self
    }
}

// impl AsLocation for &Location {
//     fn as_location(&self) -> Location {
//         **self
//     }
// }

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Locatable<T> {
    pub data: T,
    pub location: Location,
}

impl<T> Locatable<T> {
    pub fn map<U, F: FnOnce(T) -> U>(self, f: F) -> Locatable<U> {
        Locatable {
            data: f(self.data),
            location: self.location,
        }
    }
}

// impl<T: FancyError<FileId = ()>> FancyError for Locatable<T> {
//     type FileId = ();
//     fn into_diagnostic(&self) -> diagnostic::Diagnostic<Self::FileId> {
//         self.data
//             .into_diagnostic()
//             .with_labels(vec![diagnostic::Label::primary((), self.location.0)])
//     }
// }

impl<T> AsLocation for Locatable<T> {
    fn as_location(&self) -> Location {
        self.location
    }
}

macro_rules! locatable_pat {
    ($inner_pattern:pat) => {
        Locatable {
            data: $inner_pattern,
            ..
        }
    };
}

impl<T> std::ops::Deref for Locatable<T> {
    type Target = T;
    fn deref(&self) -> &T {
        &self.data
    }
}

impl<T> std::ops::DerefMut for Locatable<T> {
    fn deref_mut(&mut self) -> &mut T {
        &mut self.data
    }
}

pub type LResult<R, E> = Result<R, Locatable<E>>;

pub trait LocatableAt: Sized {
    fn at(self, location: &impl AsLocation) -> Locatable<Self>;
}

impl<T: Sized> LocatableAt for T {
    fn at(self, location: &impl AsLocation) -> Locatable<Self> {
        location.as_location().with(self)
    }
}
