use super::{ast, SemanticError, SemanticResult, Variable, VariableKind, VariableScope};
use super::{ExprType, InlineAsmCall, SingleExprType, TypedExprKind, VariableLoad, VariableStore};
use crate::common::*;
use crate::compiler::interner::Interner;
use crate::types;
use lasso::Spur;
use std::rc::Rc;

type TypedExpr<T = Values> = super::TypedExpr<T>;

impl TypedExpr<Values> {
    pub fn to_concrete(self) -> SemanticResult<TypedExpr<types::Values>> {
        let loc = self.location;
        let err_wrap = || SemanticError::NotMonotype.at(&loc);
        match self.data {
            TypedExprKind::FunctionCall {
                func,
                params,
                result,
            } => Ok(TypedExprKind::<types::Values>::FunctionCall {
                func: Box::new(func.to_concrete()?),
                params: params
                    .into_iter()
                    .map(|t| t.to_concrete())
                    .collect::<SemanticResult<_>>()?,
                result: result
                    .to_monotype()
                    .ok_or(SemanticError::NotMonotype.at(&loc))?,
            }
            .at(&loc)),
            TypedExprKind::InlineAsmCall(InlineAsmCall {
                local_types,
                asm,
                params,
                result,
            }) => Ok(
                TypedExprKind::<types::Values>::InlineAsmCall(InlineAsmCall {
                    local_types: local_types
                        .into_iter()
                        .map(|(i, t)| {
                            Ok((
                                i,
                                t.to_monotype().ok_or(SemanticError::NotMonotype.at(&loc))?,
                            ))
                        })
                        .collect::<SemanticResult<_>>()?,
                    asm,
                    params: params
                        .into_iter()
                        .map(|t| t.to_concrete())
                        .collect::<SemanticResult<Vec<_>>>()?,
                    result: result
                        .to_monotype()
                        .ok_or(SemanticError::NotMonotype.at(&loc))?,
                })
                .at(&loc),
            ),
            TypedExprKind::IntLiteral { int, int_type } => Ok(TypedExprKind::IntLiteral {
                int,
                int_type: int_type.to_monotype().ok_or_else(err_wrap)?,
            }
            .at(&loc)),
            TypedExprKind::FloatLiteral { float, float_type } => Ok(TypedExprKind::FloatLiteral {
                float,
                float_type: float_type.to_monotype().ok_or_else(err_wrap)?,
            }
            .at(&loc)),
            TypedExprKind::VariableLoad(VariableLoad::Function { id, func_type }) => {
                Ok(TypedExprKind::VariableLoad(VariableLoad::Function {
                    id,
                    func_type: func_type.to_monotype().ok_or_else(err_wrap)?,
                })
                .at(&loc))
            }
            TypedExprKind::VariableLoad(VariableLoad::Global { id, global_type }) => {
                Ok(TypedExprKind::VariableLoad(VariableLoad::Global {
                    id,
                    global_type: global_type.to_monotype().ok_or_else(err_wrap)?,
                })
                .at(&loc))
            }
            TypedExprKind::VariableLoad(VariableLoad::Local { id, var_type }) => {
                Ok(TypedExprKind::VariableLoad(VariableLoad::Local {
                    id,
                    var_type: var_type.to_monotype().ok_or_else(err_wrap)?,
                })
                .at(&loc))
            }
            TypedExprKind::VariableStore(VariableStore::Global { id, value }) => {
                Ok(TypedExprKind::VariableStore(VariableStore::Global {
                    id,
                    value: Box::new(value.to_concrete()?),
                })
                .at(&loc))
            }
            TypedExprKind::VariableStore(VariableStore::Local { id, value }) => {
                Ok(TypedExprKind::VariableStore(VariableStore::Local {
                    id,
                    value: Box::new(value.to_concrete()?),
                })
                .at(&loc))
            }
        }
    }
}

// TODO: need to figure out a better way to represent compound types in the type checker

#[derive(Clone, Debug, PartialEq)]
pub enum Type {
    /// A Concrete type
    Monotype(types::LocalType),
    /// Function type
    Function { param: Values, result: Values },
    /// Type variable
    Var(Spur),
    /// Existential type
    Exists(Spur),
    /// Forall quantifier
    Forall(Spur, Box<Type>),
}

impl Type {
    pub fn to_monotype(self) -> Option<types::LocalType> {
        match self {
            Type::Monotype(t) => Some(t),
            Type::Function { param, result } => {
                let param = param.to_monotype()?;
                let result = result.to_monotype()?;
                Some(types::LocalType::Table(types::TableType::FuncRef(
                    types::FunctionReferenceType { param, result },
                )))
            }
            _ => None,
        }
    }

    fn substitute(self, ctx: &Typechecker) -> Type {
        match self {
            Type::Var(_) | Type::Monotype(_) => self,
            Type::Function { param, result } => Type::Function {
                param: param.substitute(ctx),
                result: result.substitute(ctx),
            },

            Type::Forall(id, a) => Type::Forall(id, Box::new(a.substitute(ctx))),
            Type::Exists(_) => {
                let hole = ctx.hole(&self).unwrap();
                match &ctx.context[hole] {
                    &ContextItem::Existential(id) => Type::Exists(id),
                    ContextItem::Solved(_, ref t) => t.clone().substitute(ctx),
                    _ => unreachable!(),
                }
            }
        }
    }

    fn substitute_variable(self, replacement: Type, var: Spur) -> Type {
        match self {
            Type::Var(var_id) if var_id == var => replacement,
            Type::Var(_) | Type::Exists(_) | Type::Monotype(_) => self,
            Type::Function { param, result } => Type::Function {
                param: param.substitute_variable(replacement.clone(), var),
                result: result.substitute_variable(replacement.clone(), var),
            },
            Type::Forall(uvar_id, a) => {
                // TODO: what happens if we have variable names overlap here?
                Type::Forall(uvar_id, Box::new(a.substitute_variable(replacement, var)))
            }
        }
    }

    fn fv_contains(&self, _id: Spur) -> Type {
        // TODO: figure out how to do the α̂ ∈ FV(A) check
        unimplemented!()
    }
}

impl SingleExprType for Type {
    type Multi = Values;
}

// Subtyping stuff

// impl PartialOrd for Type {
//     fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
//         use std::cmp::Ordering;
//         match (
//             is_subtype_single(self, other),
//             is_subtype_single(other, self),
//         ) {
//             (true, true) => Some(Ordering::Equal),
//             (true, false) => Some(Ordering::Less),
//             (false, true) => Some(Ordering::Greater),
//             (false, false) => None,
//         }
//     }
// }

// fn is_subtype_multi(a: &Values, b: &Values) -> bool {
//     a.as_ref().len() == b.as_ref().len()
//         && a.as_ref()
//             .iter()
//             .zip(b.as_ref().iter())
//             .all(|(x, y)| x <= y)
// }

// fn is_subtype_single(a: &Type, b: &Type) -> bool {
//     use types::{IntegerType, LiteralType, LocalType, MemoryType, TableType};
//     match (a, b) {
//         (a, b) if a == b => true,
//         // Bottom rule
//         (&Type::Monotype(LocalType::Never), _) => true,
//         // TODO: add Top rule
//         // (_, &Type::Top) => true,

//         // Function subtyping
//         (LocalType::Table(TableType::FuncRef(f)), LocalType::Table(TableType::FuncRef(g))) => {
//             f.param >= g.param && f.result <= g.result
//         }

//         // Literals subtyping
//         (Type::Monotype(LocalType::Literal(LiteralType::Integer(n))), l) => match l {
//             Type::Monotype(LocalType::Memory(MemoryType::Integer(IntegerType::I8))) => {
//                 n.to_i8().is_some()
//             }
//             Type::Monotype(LocalType::Memory(MemoryType::Integer(IntegerType::U8))) => {
//                 n.to_u8().is_some()
//             }
//             Type::Monotype(LocalType::Memory(MemoryType::Integer(IntegerType::I16))) => {
//                 n.to_i16().is_some()
//             }
//             Type::Monotype(LocalType::Memory(MemoryType::Integer(IntegerType::U16))) => {
//                 n.to_u16().is_some()
//             }
//             Type::Monotype(LocalType::Memory(MemoryType::Integer(IntegerType::I32))) => {
//                 n.to_i32().is_some()
//             }
//             Type::Monotype(LocalType::Memory(MemoryType::Integer(IntegerType::U32))) => {
//                 n.to_u32().is_some()
//             }
//             Type::Monotype(LocalType::Memory(MemoryType::Integer(IntegerType::I64))) => {
//                 n.to_i64().is_some()
//             }
//             Type::Monotype(LocalType::Memory(MemoryType::Integer(IntegerType::U64))) => {
//                 n.to_u64().is_some()
//             }
//             _ => false,
//         },
//         (LocalType::Literal(LiteralType::Float(_)), LocalType::Memory(MemoryType::Float(_))) => {
//             true
//         }
//         // Everything else false
//         _ => false,
//     }
// }

#[derive(Clone, Debug, PartialEq)]
pub struct Values {
    types: Vec<Type>,
}

impl Values {
    pub fn to_monotype(self) -> Option<types::Values> {
        self.types.into_iter().map(Type::to_monotype).collect()
    }

    pub fn from_single(t: Type) -> Values {
        Values { types: vec![t] }
    }

    fn substitute(self, ctx: &Typechecker) -> Values {
        self.types.into_iter().map(|t| t.substitute(ctx)).collect()
    }

    fn substitute_variable(self, replacement: Type, var: Spur) -> Values {
        self.types
            .into_iter()
            .map(|t| t.substitute_variable(replacement.clone(), var))
            .collect()
    }
}

impl From<Type> for Values {
    fn from(t: Type) -> Self {
        Values::from_single(t)
    }
}

impl AsRef<[Type]> for Values {
    fn as_ref(&self) -> &[Type] {
        self.types.as_ref()
    }
}

impl AsMut<[Type]> for Values {
    fn as_mut(&mut self) -> &mut [Type] {
        self.types.as_mut()
    }
}

impl ExprType for Values {
    type Single = Type;
}

impl FromIterator<Type> for Values {
    fn from_iter<T: IntoIterator<Item = Type>>(iter: T) -> Self {
        Values {
            types: iter.into_iter().collect(),
        }
    }
}

enum ContextItem {
    Variable(Spur),       // α, β
    Existential(Spur),    // α̂, β̂
    Solved(Spur, Type),   // α̂ = τ, β̂ = σ
    Marker(Spur),         // ▸α̂, ▸β̂
    TypedVar(Spur, Type), // x : A
}

impl ContextItem {
    fn id(&self) -> Spur {
        match self {
            &ContextItem::Variable(id)
            | &ContextItem::Existential(id)
            | &ContextItem::Solved(id, _)
            | &ContextItem::Marker(id)
            | &ContextItem::TypedVar(id, _) => id,
        }
    }
}

pub struct Typechecker {
    interner: Interner,
    loc: Location,
    scope: Rc<VariableScope>,
    context: Vec<ContextItem>,
}

impl Typechecker {
    pub fn new(scope: Rc<VariableScope>, interner: Interner, loc: Location) -> Typechecker {
        Typechecker {
            interner,
            loc,
            scope,
            context: Vec::new(),
        }
    }

    fn find_var(&self, var_id: Spur) -> Option<usize> {
        for (i, item) in self.context.iter().enumerate() {
            match item {
                &ContextItem::Variable(id) if id == var_id => {
                    return Some(i);
                }
                _ => {}
            }
        }
        None
    }

    fn find_exvar(&self, id: Spur) -> Option<usize> {
        for (i, item) in self.context.iter().enumerate() {
            if let &ContextItem::Existential(marker_id) = item {
                if marker_id == id {
                    return Some(i);
                }
            } else if let &ContextItem::Solved(marker_id, _) = item {
                if marker_id == id {
                    return None;
                }
            }
        }
        None
    }

    fn find_marker(&self, id: Spur) -> Option<usize> {
        for (i, item) in self.context.iter().enumerate() {
            if let &ContextItem::Marker(marker_id) = item {
                if marker_id == id {
                    return Some(i);
                }
            }
        }
        None
    }

    fn hole(&self, t: &Type) -> Option<usize> {
        match t {
            &Type::Exists(ex_id) => {
                for (i, item) in self.context.iter().enumerate() {
                    match item {
                        &ContextItem::Solved(id, _) | &ContextItem::Existential(id)
                            if id == ex_id =>
                        {
                            return Some(i);
                        }
                        _ => {}
                    }
                }
                None
            }
            &Type::Var(var_id) => self.find_var(var_id),
            _ => None,
        }
    }

    fn introduce_exvar(&mut self) -> Spur {
        let new_existential = self.interner.generate_symbol("exvar-");
        self.context.push(ContextItem::Marker(new_existential));
        self.context.push(ContextItem::Existential(new_existential));
        new_existential
    }

    fn is_multi_wellformed(&mut self, t: &Values) -> SemanticResult<()> {
        for t in t.as_ref() {
            self.is_wellformed(t)?;
        }
        Ok(())
    }

    fn is_wellformed(&mut self, t: &Type) -> SemanticResult<()> {
        match t {
            // UnitWF rule
            Type::Monotype(_) => {}
            // ArrowWF rule
            Type::Function { param, result } => {
                self.is_multi_wellformed(param)?;
                self.is_multi_wellformed(result)?;
            }
            // UvarWF rule
            &Type::Var(_id) => {
                self.hole(t)
                    .ok_or(SemanticError::UnqualifiedTypeVariable.at(&self.loc))?;
            }
            // EvarWF rule & SolvedEvarWF rule
            &Type::Exists(_id) => {
                self.hole(t)
                    .ok_or(SemanticError::UnqualifiedTypeVariable.at(&self.loc))?;
            }
            // ForallWF rule
            &Type::Forall(id, ref a) => {
                self.context.push(ContextItem::Variable(id));
                self.is_wellformed(a)?;
                self.context.pop();
            }
        };
        Ok(())
    }

    fn subtype(&mut self, a: Type, b: Type) -> SemanticResult<bool> {
        match (a, b) {
            // <:Var rule
            (Type::Var(a_id), Type::Var(b_id)) if a_id == b_id => {
                // self.hole(a);
                Ok(true)
            }
            // <:Unit rule
            (Type::Monotype(t), Type::Monotype(u)) => Ok(t <= u),
            // <:ExVar rule
            (Type::Exists(a_id), Type::Exists(b_id)) if a_id == b_id => Ok(true),
            // <:-> rule
            // TODO: add rules for subtyping with monotype functions
            (
                Type::Function {
                    param: a1,
                    result: a2,
                },
                Type::Function {
                    param: b1,
                    result: b2,
                },
            ) => {
                self.subtype_multi(b1, a1)?;
                let a2 = a2.substitute(self);
                let b2 = b2.substitute(self);
                self.subtype_multi(a2, b2)?;
                Ok(true)
            }
            // <:∀L rule
            (Type::Forall(a_var, a), b) => {
                let exvar = self.introduce_exvar();
                let a = a.substitute_variable(Type::Exists(exvar), a_var);
                let res = self.subtype(a, b)?;
                self.context.truncate(self.find_marker(exvar).unwrap());
                Ok(res)
            }
            // <:∀R rule
            (a, Type::Forall(b_var, b)) => {
                self.context.push(ContextItem::Variable(b_var));
                let res = self.subtype(a, *b)?;
                self.context.truncate(self.find_var(b_var).unwrap());
                Ok(res)
            }
            // InstantiateL rule
            (Type::Exists(â), a) => {
                // let hole = self.hole(Type::Exists(â)).unwrap();
                self.inst_l(â, a)?;
                Ok(true)
            }
            // InstantiateR rule
            (a, Type::Exists(â)) => {
                self.inst_r(a, â)?;
                Ok(true)
            }
            (a, b) => Err(SemanticError::UnexpectedType {
                expected: b,
                found: a,
            }
            .at(&self.loc)),
        }
    }

    fn subtype_multi(&mut self, a: Values, b: Values) -> SemanticResult<bool> {
        for (at, bt) in a.types.into_iter().zip(b.types) {
            let res = self.subtype(at, bt)?;
            if !res {
                return Ok(false);
            }
        }
        Ok(true)
    }

    fn resolve_type_with_scope(
        &mut self,
        t: &ast::Type,
        scope: Rc<VariableScope>,
    ) -> SemanticResult<Type> {
        match &t.data {
            &ast::TypeKind::Variable(id) => scope
                .lookup_type(id)
                .cloned()
                .ok_or(SemanticError::UnboundVariable.at(t)),
            ast::TypeKind::Function { param, result } => Ok(Type::Function {
                param: param
                    .iter()
                    .map(|e| self.resolve_type_with_scope(e, scope.clone()))
                    .collect::<SemanticResult<Values>>()?,
                result: result
                    .iter()
                    .map(|e| self.resolve_type_with_scope(e, scope.clone()))
                    .collect::<SemanticResult<Values>>()?,
            }),
            ast::TypeKind::Forall { vars, ty } => {
                let new_ids = vars
                    .iter()
                    .map(|_id| self.interner.generate_symbol("uvar-"))
                    .collect::<Vec<_>>();
                let mut sub_scope = VariableScope::new_from(&scope);
                for (&var, &new_id) in vars.iter().zip(new_ids.iter()) {
                    sub_scope.define_type(var, Type::Var(new_id), t.as_location())?;
                }
                let mut t = self.resolve_type_with_scope(ty, Rc::new(sub_scope))?;
                for (&_var, &new_id) in vars.iter().rev().zip(new_ids.iter().rev()) {
                    t = Type::Forall(new_id, Box::new(t));
                }
                Ok(t)
            }
        }
    }

    pub fn resolve_type(&mut self, e: &ast::Type) -> SemanticResult<Type> {
        self.resolve_type_with_scope(e, self.scope.clone())
    }

    pub fn resolve_multi_type<'h>(
        &mut self,
        t: impl IntoIterator<Item = &'h ast::Type>,
    ) -> SemanticResult<Values> {
        t.into_iter()
            .map(|t| self.resolve_type(t))
            .collect::<SemanticResult<Values>>()
    }

    pub fn check(&mut self, e: &ast::Expr, t: &Values) -> SemanticResult<TypedExpr> {
        let result = self.check_multi(std::slice::from_ref(e), t)?;
        match <[TypedExpr; 1]>::try_from(result) {
            Ok([expr]) => Ok(expr),
            _ => panic!("wrong number of expressions"),
        }
    }

    pub fn check_multi(
        &mut self,
        es: &[ast::Expr],
        t: &Values,
    ) -> SemanticResult<Vec<TypedExpr<Values>>> {
        // Sub rule

        let mut exprs = es
            .iter()
            .map(|e| self.synth(e))
            .collect::<SemanticResult<Vec<TypedExpr>>>()?;

        let args_type = exprs
            .iter()
            .flat_map(|te| te.as_multi_type().types)
            .collect::<Values>();

        for (at, bt) in args_type.as_ref().iter().zip(t.as_ref()) {
            self.subtype(at.clone(), bt.clone())?;
        }

        let mut i = 0;
        let specified_types = t.as_ref();
        for expr in &mut exprs {
            let type_slice = expr.as_type_slice_mut();
            type_slice.clone_from_slice(&specified_types[i..i + type_slice.len()]);
            i += type_slice.len();
        }

        Ok(exprs)

        // if &args_type <= t {
        //
        // } else {
        //     Err(SemanticError::UnexpectedType {
        //         expected: t.clone(),
        //         found: args_type,
        //     }
        //     .at(&es
        //         .iter()
        //         .map(|e| e.location)
        //         .collect::<Option<Location>>()
        //         .expect("Could not find location for error message")))
        // }
    }

    fn lookup_var(&self, id: Spur) -> Option<VariableKind> {
        let var = self.context.iter().rev().find_map(|ci| match ci {
            ContextItem::TypedVar(var_id, ty) if *var_id == id => Some(ty),
            _ => None,
        });
        if let Some(var_type) = var {
            Some(VariableKind::Local(var_type.clone()))
        } else {
            self.scope.lookup(id).map(Variable::variable).cloned()
        }
    }

    pub fn synth(&mut self, e: &ast::Expr) -> SemanticResult<TypedExpr> {
        match &e.data {
            // Anno rule
            ast::ExprType::TypeAnnotation(ast::expr::TypeAnnotation { t, e }) => {
                let t = self.resolve_multi_type(t)?;
                self.check(e.as_ref(), &t)
            }
            // Var rule
            &ast::ExprType::Variable { id } => {
                Ok(TypedExprKind::VariableLoad(match self.lookup_var(id) {
                    Some(VariableKind::Local(t)) => VariableLoad::Local {
                        id,
                        var_type: t.clone(),
                    },
                    Some(VariableKind::Function(f)) => VariableLoad::Function {
                        id,
                        func_type: f.clone(),
                    },
                    Some(VariableKind::Global { ty, .. }) => VariableLoad::Global {
                        id,
                        global_type: Type::Monotype(ty.clone()),
                    },
                    Some(VariableKind::Type(_)) => {
                        return Err(SemanticError::TypeUsedInExpressionContext.at(e))
                    }
                    None => return Err(SemanticError::UnboundVariable.at(e)),
                })
                .at(e))

                //
            }
            // Not a pre-existing rule
            //
            // (x:A) ∈ Γ    Γ ⊢ e <== A ⊣ Δ
            // ----------------------------
            //   Γ ⊢ (set! x e) ==> 1 ⊣ Δ
            ast::ExprType::VariableSet(set_expr) => Ok(TypedExprKind::VariableStore(
                match self.lookup_var(set_expr.id) {
                    Some(VariableKind::Local(t)) => VariableStore::Local {
                        id: set_expr.id,
                        value: Box::new(
                            self.check(&set_expr.value, &Values::from_single(t.clone()))?,
                        ),
                    },
                    Some(VariableKind::Global { mutable: true, ty }) => VariableStore::Global {
                        id: set_expr.id,
                        value: Box::new(self.check(
                            &set_expr.value,
                            &Values::from_single(Type::Monotype(ty.clone())),
                        )?),
                    },
                    Some(VariableKind::Function(_))
                    | Some(VariableKind::Global { mutable: false, .. }) => {
                        return Err(SemanticError::SetImmutable.at(e))
                    }
                    Some(VariableKind::Type(_)) => {
                        return Err(SemanticError::TypeUsedInExpressionContext.at(e))
                    }
                    None => return Err(SemanticError::UnboundVariable.at(e)),
                },
            )
            .at(e)),
            // -->E rule
            ast::ExprType::FunctionCall(func_call) => {
                let callee = self.synth(&func_call.func)?;

                self.synth_app(callee, &func_call.arguments)

                // let func_type = match callee.as_single_type() {
                //     Some(LocalType::Table(TableType::FuncRef(func_type))) => func_type,
                //     _ => {
                //         return Err(SemanticError::CalleeWasNotFunction.at(func_call.func.as_ref()))
                //     }
                // };

                // Ok(TypedExprKind::FunctionCall {
                //     func: Box::new(callee),
                //     params: self.check_multi(&func_call.arguments, &func_type.param)?,
                //     result: func_type.result.clone(),
                // }
                // .at(e))
                // }
                // }
            }
            // Inline assembly rule
            //
            //               Γ ⊢ arguments <== P ⊣ Δ
            // ------------------------------------------------------
            // Γ ⊢ (%inline-asm (P) (R) () (asm) arguments) ==> r ⊣ Δ
            ast::ExprType::InlineAsm(inline_asm_call) => {
                // TODO: typechech the assembly itself. Maybe by generating a
                // tiny wasm module with just one functoin containing that code
                // and then verifying it with external tools.

                let param_types = self.resolve_multi_type(inline_asm_call.params.iter())?;

                let params = self.check_multi(&inline_asm_call.arguments, &param_types)?;

                Ok(TypedExprKind::InlineAsmCall(InlineAsmCall {
                    // param_types: inline_asm_call
                    //     .params
                    //     .iter()
                    //     .map(|(id, ty)| Ok((*id, self.scope.resolve_type(ty)?.clone())))
                    //     .collect::<SemanticResult<Vec<(Spur, LocalType)>>>()?,
                    local_types: inline_asm_call
                        .locals
                        .iter()
                        .map(|(id, ty)| Ok((*id, self.resolve_type(ty)?.clone())))
                        .collect::<SemanticResult<Vec<(Spur, Type)>>>()?,
                    asm: inline_asm_call.asm.clone(),
                    params,
                    result: self.resolve_multi_type(&inline_asm_call.results)?,
                })
                .at(e))
            }
            &ast::ExprType::Literal(ast::expr::Literal::Float(float)) => {
                Ok(TypedExprKind::FloatLiteral {
                    float,
                    float_type: Type::Monotype(types::LocalType::Literal(
                        types::LiteralType::Float(float),
                    )),
                }
                .at(e))
            }
            ast::ExprType::Literal(ast::expr::Literal::Integer(n)) => {
                Ok(TypedExprKind::IntLiteral {
                    int: n.clone(),
                    int_type: Type::Monotype(types::LocalType::Literal(
                        types::LiteralType::Integer(n.clone()),
                    )),
                }
                .at(e))
            }
            ast::ExprType::Literal(ast::expr::Literal::String(_s)) => unimplemented!(),
        }
    }

    fn synth_app(
        &mut self,
        mut callee: TypedExpr<Values>,
        arguments: &[ast::Expr],
    ) -> SemanticResult<TypedExpr> {
        // TODO: maybe later this should deal with some `Callable` abstract type
        // that can represent more than function references (possibly closures)
        match callee.as_single_type() {
            // ∀App rule
            Some(Type::Forall(var, ty)) => {
                let new_exvar = self.interner.generate_symbol("exvar-");
                self.context.push(ContextItem::Existential(new_exvar));
                callee.as_type_slice_mut()[0] =
                    ty.substitute_variable(Type::Exists(new_exvar), var);
                self.synth_app(callee, arguments)
            }
            // α̂App rule
            Some(Type::Exists(ex_var)) => {
                let _hole = self.find_exvar(ex_var);
                let _ex_var_1 = self.interner.generate_symbol("exvar-");
                let _ex_var_2 = self.interner.generate_symbol("exvar-");
                // TODO: we need variadic generic functions now :/
                // should produce a variadic function and where â1 and â2 can be variadic arguments
                unimplemented!()
                // self.context.splice(
                //     hole..hole + 1,
                //     [
                //         ContextItem::Existential(ex_var_2),
                //         ContextItem::Existential(ex_var_1),
                //         ContextItem::Solved(
                //             ex_var,
                //             Type::Function {
                //                 param: (),
                //                 result: (),
                //             },
                //         ),
                //     ],
                // )
            }
            // ->App rule
            Some(Type::Function { param, result }) => {
                let params = self.check_multi(arguments, &param)?;
                Ok(TypedExprKind::FunctionCall {
                    func: Box::new(callee),
                    params,
                    result,
                }
                .at(&self.loc))
            }
            // TODO: add a rule for monotype functions
            _ => Err(SemanticError::CalleeWasNotFunction.at(&self.loc)),
        }
    }

    fn inst_l(&mut self, _exvar: Spur, _b: Type) -> SemanticResult<()> {
        unimplemented!()
    }
    fn inst_r(&mut self, _a: Type, _exvar: Spur) -> SemanticResult<()> {
        unimplemented!()
    }

    pub fn check_function(
        &mut self,
        func: &ast::FunctionDefinition,
    ) -> SemanticResult<(Values, TypedExpr)> {
        // Implements the ->I rule for top level function definitions

        let ty = self.resolve_type(&func.func_type)?;

        // TODO: ignore type variables for now
        // let mut type_vars = Vec::new();
        // while let Type::Forall(v, t) = ty {
        //     type_vars.push(self.introduce_exvar());
        //     ty = *t;
        // }

        fn get_function(_checker: &mut Typechecker, t: Type) -> (Values, Values) {
            match t {
                Type::Function { param, result } => (param, result),
                Type::Monotype(types::LocalType::Table(types::TableType::FuncRef(
                    types::FunctionReferenceType {
                        param: _,
                        result: _,
                    },
                ))) => {
                    unimplemented!()
                }
                _ => unimplemented!(),
            }
        }

        let (param_types, result_type) = get_function(self, ty);

        for (param_type, param_name) in param_types.as_slice().iter().zip(&func.params) {
            self.context
                .push(ContextItem::TypedVar(param_name.data, param_type.clone()));
        }

        let body = self.check(&func.body, &result_type)?;

        Ok((param_types, body))
    }
}
