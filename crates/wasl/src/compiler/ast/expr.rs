use super::{ParserContext, SyntaxError, SyntaxResult, Type};
use crate::common::*;
use crate::sexpr::{SExpr, Syntax};
use lasso::Spur;

#[derive(Clone, Debug)]
pub enum Literal {
    Integer(num::BigInt),
    Float(f64),
    String(String),
}

#[derive(Clone, Debug)]
pub struct FunctionCall {
    pub func: Box<Expr>,
    pub arguments: Vec<Expr>,
}

#[derive(Clone, Debug)]
pub struct InlineAsmCall {
    pub params: Vec<Type>,
    pub results: Vec<Type>,
    pub locals: Vec<(Spur, Type)>,
    pub asm: Vec<Syntax>,
    pub arguments: Vec<Expr>,
}

pub const INLINE_ASM_SYMBOL: &str = "%inline-asm";

impl InlineAsmCall {
    // pub fn param_types(&self) -> Vec<Type> {
    //     self.params.iter().map(|(_, t)| t.clone()).collect()
    // }

    /// Parse an inline assembly call from s-expression
    ///
    /// Syntax:
    /// ```plain
    /// (%inline-asm (<param_type>...) (<result_type>...) ((<local_id> <local_type>)...)
    ///     (<body>...)
    ///     <arguments>...)
    /// ```
    fn parse(source: &Syntax, ctx: &ParserContext) -> SyntaxResult<InlineAsmCall> {
        let inline_asm_symbol = ctx.interner.borrow().get(INLINE_ASM_SYMBOL).unwrap();

        match source.as_list() {
            Some([syntax_pat!(SExpr::Symbol(sym)), params, results, locals, body, args @ ..])
                if *sym == inline_asm_symbol =>
            {
                let params = params
                    .as_list()
                    .ok_or(SyntaxError::InvalidInlineAsmSyntax.at(params))?
                    .iter()
                    .map(|t| Type::parse(t, ctx))
                    //     match s.as_list() {
                    //     Some([syntax_pat!(SExpr::Symbol(input_id)), input_type]) => {
                    //         Type::parse(input_type, ctx).map(|t| (*input_id, t))
                    //     }
                    //     _ => Err(SyntaxError::InvalidInlineAsmSyntax.at(s)),
                    // })
                    .collect::<SyntaxResult<Vec<Type>>>()?;

                let results = results
                    .as_list()
                    .ok_or(SyntaxError::InvalidInlineAsmSyntax.at(results))?
                    .iter()
                    .map(|t| Type::parse(t, ctx))
                    .collect::<SyntaxResult<Vec<Type>>>()?;

                let locals = locals
                    .as_list()
                    .ok_or(SyntaxError::InvalidInlineAsmSyntax.at(locals))?
                    .iter()
                    .map(|s| match s.as_list() {
                        Some([syntax_pat!(SExpr::Symbol(local_id)), local_type]) => {
                            Type::parse(local_type, ctx).map(|t| (*local_id, t))
                        }
                        _ => Err(SyntaxError::InvalidInlineAsmSyntax.at(s)),
                    })
                    .collect::<SyntaxResult<Vec<(Spur, Type)>>>()?;

                let body = body
                    .as_list()
                    .ok_or(SyntaxError::InvalidInlineAsmSyntax.at(body))?
                    .to_owned();

                let args = args
                    .iter()
                    .map(|s| Expr::parse(s, ctx))
                    .collect::<SyntaxResult<Vec<Expr>>>()?;

                Ok(InlineAsmCall {
                    params,
                    results,
                    locals,
                    asm: body,
                    arguments: args,
                })
            }
            _ => Err(SyntaxError::InvalidInlineAsmSyntax.at(source)),
        }
    }

    // fn to_wat_typecheck(&self, ctx: &ParserContext) -> String {}
}

#[derive(Clone, Debug)]
pub struct TypeAnnotation {
    pub t: Vec<Type>,
    pub e: Box<Expr>,
}

pub const TYPE_ANNOTATION_SYMBOL: &str = ":";

impl TypeAnnotation {
    fn parse(source: &Syntax, ctx: &ParserContext) -> SyntaxResult<TypeAnnotation> {
        let annotation_symbol_id = ctx.interner.borrow().get(TYPE_ANNOTATION_SYMBOL).unwrap();

        // TODO: should change the order of arguments here
        match source.as_list() {
            Some(&[syntax_pat!(SExpr::Symbol(id)), ref expr, ref types @ ..])
                if id == annotation_symbol_id =>
            {
                Ok(TypeAnnotation {
                    t: types
                        .iter()
                        .map(|e| Type::parse(e, ctx))
                        .collect::<SyntaxResult<Vec<Type>>>()?,
                    e: Box::new(Expr::parse(expr, ctx)?),
                })
            }
            _ => Err(SyntaxError::InvalidTypeAnnotationSyntax.at(source)),
        }
    }
}

#[derive(Clone, Debug)]
pub struct SetExpression {
    pub id: Spur,
    pub value: Box<Expr>,
}

pub const SET_SYMBOL: &str = "set!";

impl SetExpression {
    fn parse(source: &Syntax, ctx: &ParserContext) -> SyntaxResult<SetExpression> {
        let set_symbol = ctx.interner.static_symbol(SET_SYMBOL);

        match source.as_list() {
            Some(
                &[syntax_pat!(SExpr::Symbol(set_id)), syntax_pat!(SExpr::Symbol(var_id)), ref expr],
            ) if set_id == set_symbol => Ok(SetExpression {
                id: var_id,
                value: Box::new(Expr::parse(expr, ctx)?),
            }),
            _ => Err(SyntaxError::InvalidVariableSet.at(source)),
        }
    }
}

#[derive(Clone, Debug)]
pub enum ExprType {
    Literal(Literal),
    Variable { id: Spur },
    VariableSet(SetExpression),
    FunctionCall(FunctionCall),
    InlineAsm(InlineAsmCall),
    TypeAnnotation(TypeAnnotation),
}

pub type Expr = Locatable<ExprType>;

impl Expr {
    pub fn parse(source: &Syntax, ctx: &ParserContext) -> SyntaxResult<Expr> {
        Ok(match source.sexpr() {
            SExpr::List(items) => {
                let inline_asm_symbol = ctx.interner.static_symbol(INLINE_ASM_SYMBOL);
                let type_annotation_symbol = ctx.interner.static_symbol(TYPE_ANNOTATION_SYMBOL);
                let set_symbol = ctx.interner.static_symbol(SET_SYMBOL);

                if items.is_empty() {
                    return Err(SyntaxError::InvalidExprSyntax.at(source));
                }

                match items[0].sexpr() {
                    SExpr::Symbol(sym) if *sym == inline_asm_symbol => {
                        ExprType::InlineAsm(InlineAsmCall::parse(source, ctx)?)
                    }
                    SExpr::Symbol(sym) if *sym == type_annotation_symbol => {
                        ExprType::TypeAnnotation(TypeAnnotation::parse(source, ctx)?)
                    }
                    SExpr::Symbol(sym) if *sym == set_symbol => {
                        ExprType::VariableSet(SetExpression::parse(source, ctx)?)
                    }
                    _ => ExprType::FunctionCall(FunctionCall {
                        func: Box::new(Expr::parse(&items[0], ctx)?),
                        arguments: items[1..]
                            .iter()
                            .map(|a| Expr::parse(a, ctx))
                            .collect::<SyntaxResult<Vec<Expr>>>()?,
                    }),
                }
            }
            SExpr::Integer(i) => ExprType::Literal(Literal::Integer(i.clone())),
            SExpr::Float(f) => ExprType::Literal(Literal::Float(*f)),
            SExpr::String(s) => ExprType::Literal(Literal::String(s.clone())),
            SExpr::Symbol(id) => ExprType::Variable { id: *id },
        }
        .at(source))
    }
}
