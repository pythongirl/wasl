## New Syntax Ideas

begin syntax: `(begin <expr> ... <expr>)`
synth begin:
   for e in body[..-1] { check(e, Top)? }
   return synth(last)

let syntax: `(let (<bindings> <expr>) <body> ...)`

~~set syntax: `(set! <vars> ... <expr>)`~~ Implemented


return syntax: `(return <expr>...)`
typechecking:
check to make sure expr matches function return type and synthesize bottom




### Could be functoins later

Values syntax: `(values <expr> ...)`
- just returns the arguments

Drop syntax: `(drop <expr> ...)`
- takes values and executes them for side effects


